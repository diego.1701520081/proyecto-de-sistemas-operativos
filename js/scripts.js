  //VARIABLES GLOBALES
  var pids=[] //contiene los pids de los procesos 
  var procesos=[] //contiene cada proceso
  var indices=[] //contiene los indices de cada proceso
  var mat=[]  //Es la matriz del proceso "actual" con todo el codigo segmentado
  var pid=-1 // Es el pid del proceso que se ejecuta en el momento
  var lineaActual=0 //contiene el indice de la linea actual que se esta ejecutando del codigo
  var variables=[] //Contiene todas la variables de los diferentes procesos
  var banderaInput=false //bandera para frenar la ejecucion mientras el usuario ingresa un dato
  var indice=0   //Contiene la linea actual de ejecucion
  var modalidad="ninguna" //la modalidad puede variar a pasos y continua
  const elem = document.getElementById('modal1');
  const instance = M.Modal.init(elem, {dismissible: false});
  var ejecucionDetenida=false //evita que se cargue un proceso si solo se detuvo la ejecucion para recibir datos
  
  document.getElementById("cod").innerHTML = "nueva unidad I 1\n"+
  "nueva num I 5\n"+"nueva resul I 1\n"+"nueva ms C El_Factorial_es:\n"+
  "lea num\n"+"cargue num\n"+"multiplique resul\n"+"almacene resul\n"+"cargue num\n"+
  "reste unidad\n"+"almacene num\n"+"vayasi cic fin\n"+"etiqueta cic 5\n"+
  "etiqueta fin 14\n"+"muestre ms\n"+"imprima resul\n"+"retorne"
  
  //Realiza toda la compilacion de el codigo revisando toda su sintaxis
  function compilar()
  {
    let text = document.getElementById("cod").value  //recibimos el texto del input
    let vectText=text.split("\n")  //vector con cada linea de el codigo
    let mat=[] //matriz con cada palabra del codigo
    for(linea of vectText)
    {
      mat.push(linea.split(" "))
    }
    let valOp=comprobarOperacion(mat)
    if(valOp!=true) //Si alguna operacion tiene alguna falla
    {
      document.getElementById("err").innerHTML = "La operacion "+valOp+" no existe!!!"
    }
    else
    {
      document.getElementById("err").innerHTML += "Compilacion exitosa!!!\n"
      this.procesos.push(mat)
      this.indices.push(0)
      if(this.pids.length==0) //Comenzamos a guardar cada pid de cada proceso
      {
        variables.push(["acumulador","I",0,0]) //Aprovechamos y guardamos el acumulador
        this.pids.push(0)
      }
      else
      {
        variables.push(["acumulador","I",0,this.pids.length]) //Aprovechamos y guardamos el acumulador
        this.pids.push(this.pids.length)
      }
    }
  } 
  
  //Asigna un valor a una variable guardada
  function asignarValor(variable,valor)
  {
    for(i=0;i<this.variables.length;i++)
    {
      if(this.variables[i][0]==variable && this.variables[i][3]==this.pid)
      {
        this.variables[i][2]=valor
      }
    }
  }
  
  //Obtener un valor de una variable guardada
  function obtenerValor(variable)
  {
    for(i=0;i<this.variables.length;i++)
    {
      if(this.variables[i][0]==variable && this.variables[i][3]==this.pid)
      {
        return this.variables[i][2]
      }
    }
  }
  
  //Continua la ejecucion despues de pasar el LEA
  function continuarEjecucion()
  {
    banderaInput = false
    if(this.mat[indice][0]=="lea")
    {
      var elem = document.getElementById('icon_prefix').value
      asignarValor(this.mat[indice][1],elem)
    }
    if(this.mat[this.indice][0]!="vayasi")
    {
      this.indice=this.indice+1
    }
    instance.close();
    ejecutar("")
  }
  
  //Carga el proceso correcto para poder ser ejecutado posteriormente
  //Siguiendo el metodo de planificacion elegido por el usuario
  //Retorna False si ya no es posible cargar un proceso
  function cargarProceso()
  {
    if(banderaInput == false && ejecucionDetenida == false)
    {
      //por fines practicos implementaremos el fcfs
      if(pids.length <= pid+1)
      {
        return false
      }
      if(pid == -1) //Si no se a asignado un pid
      {
        pid=this.pids[0]
        indice=this.indices[0]
      }
      else
      {
        pid = pids[pid + 1]
        indice = indices[pid]
      }
      mat=this.procesos[this.pid]
      return true
    }
    else if(banderaInput == false && ejecucionDetenida == true)
    {
      ejecucionDetenida = false
      return true
    }
    return false
  }
  
  //Realiza la ejecucion de cada linea en continuo
  //Cuando aparece un Lea la ejecucion se detiene, pero se guarda el la linea actual de ejecucion
  function ejecutar(modalidad)
  {
    let html=""
    while(cargarProceso())
    {
      let i = indice
      if(this.modalidad == "ninguna")//Si aun no se a guardado la modalidad, lo hacemos
      {
        this.modalidad = modalidad
      }
      while(i < mat.length && banderaInput==false)
      {
        if(this.modalidad=="pasos") //si se ejecuta paso a paso solo realizamos la ejecucion de una linea
        {
          banderaInput=true //activando la bandera que detiene este ciclo
          ejecucionDetenida = true
          document.getElementById("tituloModal").innerHTML = "<h3 id='tituloModal'>Instruccion:</h3>"
          html+="<p>"
          for(r=0;r<this.mat[i].length;r++)
          {
            html+=this.mat[i][r]+" "
          }
          html+="</p></div>"
          document.getElementById("textInput").innerHTML = html;
          if(this.mat[i][0]!="lea")
          {
            instance.open();
          }   
        }
        if(this.mat[i][0]=="nueva")
        {
          variables.push([this.mat[i][1],this.mat[i][2],this.mat[i][3],pid]) //Guardamos una nueva variable
        }
        else if(this.mat[i][0]=="lea")
        {
          if(this.modalidad!="pasos")
          {
            document.getElementById("tituloModal").innerHTML = "<h3 id='tituloModal'>Entrada de datos:</h3>" 
          }
          html+="<div class='input-field col s6'><i class='material-icons prefix'>assignment</i>"
          html+="<input id='icon_prefix' type='text' class='validate'>"
          html+="<label for='icon_prefix'>Valor para "+this.mat[i][1]+"</label></div>"
          document.getElementById("textInput").innerHTML = html;
          instance.open();
          banderaInput = true  //Activando semaforos
          ejecucionDetenida = true
        }
        else if(this.mat[i][0]=="cargue")
        {
          asignarValor("acumulador",obtenerValor(this.mat[i][1]))
        }
        else if(this.mat[i][0]=="sume")
        {
          asignarValor("acumulador",obtenerValor("acumulador")+obtenerValor(this.mat[i][1]))
        }
        else if(this.mat[i][0]=="reste")
        {
          asignarValor("acumulador",obtenerValor("acumulador")-obtenerValor(this.mat[i][1]))
        }
        else if(this.mat[i][0]=="multiplique")
        {
          asignarValor("acumulador",obtenerValor("acumulador")*obtenerValor(this.mat[i][1]))
        }
        else if(this.mat[i][0]=="divida")
        {
          asignarValor("acumulador",obtenerValor("acumulador")/obtenerValor(this.mat[i][1]))
        }
        else if(this.mat[i][0]=="almacene")
        {
          asignarValor(this.mat[i][1],obtenerValor("acumulador"))
        }
        else if(this.mat[i][0]=="muestre")
        {
          document.getElementById("sal").innerHTML = obtenerValor(this.mat[i][1])
        }
        else if(this.mat[i][0]=="imprima")
        {
          document.getElementById("imp").innerHTML += obtenerValor(this.mat[i][1])+"\n"
        }
        else if(this.mat[i][0]=="vayasi")
        {
          if(obtenerValor("acumulador")<0)
          {
            i=parseInt(this.mat[i+1][2])
          }
          else if(obtenerValor("acumulador")>0)
          {
            i=parseInt(this.mat[i+1][2])
          }
          else
          {
            i=i+2
          }
        }    
        this.indice=i
        i++
      }
      crearTablaDeVariables()
    }
  }
  
  //Comprueba que la linea tenga una operacion existe
  //retorna true todo esta correcto, de lo contrario retornara el error cometido
  let comprobarOperacion=(mat)=>
  {
    for(let i=0; i<mat.length ;i++)
    {
      if(mat[i][0]!="cargue" && mat[i][0]!="almacene" && mat[i][0]!="nueva" && mat[i][0]!="etiqueta" && 
      mat[i][0]!="lea" && mat[i][0]!="sume" && mat[i][0]!="reste" && mat[i][0]!="multiplique" &&
      mat[i][0]!="divida" && mat[i][0]!="potencia" && mat[i][0]!="modulo" && mat[i][0]!="concatene" &&
      mat[i][0]!="elimine" && mat[i][0]!="extraiga" && mat[i][0]!="y" && mat[i][0]!="o" && 
      mat[i][0]!="no" && mat[i][0]!="muestre" && mat[i][0]!="imprima" && mat[i][0]!="retorne" 
      && mat[i][0]!="vayasi")
      {
        return mat[i][0]     
      }
    }
    return true
  }
  
  let crearTablaDeVariables=()=>
  {
    let tamMemoria=parseInt(document.getElementById("memoria").value)
    html ="<table id='posMem' class='responsive-table'><thead><tr><th> Dir </th><th>Tipo</th><th>Nombre</th><th>Valor</th><th>Pid</th></tr></thead><tbody>";
    let r=0;  //Para recorrer cada variable
    for (var i=0; i<tamMemoria; i++) 
    {
      if(i<parseInt(document.getElementById("kernel").value)) //Reservamos lo del kernel
      {
        html+="<tr><td>"+i+"</td><td>--</td><td>Reservado para kernel</td><td>--</td><td>--</td></tr>"
      }
      else if(r<this.variables.length)  //Guardamos cada instruccion
      {
        html+="<tr><td>"+i+"</td><td>"+this.variables[r][1]+"</td><td>"+this.variables[r][0]+"</td><td>"+this.variables[r][2]+"</td><td>"+this.variables[r][3]+"</td></tr>"
        r++
      }
      else   //El resto queda vacio
      {
        html+="<tr><td>"+i+"</td><td>--</td><td>--Vacio--</td><td>--</td><td>--</td></tr>"
      }   
    }
    html += "</tbody></table>";
    document.getElementById("containerVar").innerHTML = html;
  }
  
  //Iniciacion de componentes de materialize
  
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, {});
  }); 
  
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, {});
  });
  
  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, {});
  });
  
  
  
  
  
  